import React from 'react';
import { render } from '@testing-library/react';
import App from '../App';

test('renders DASHBOARD link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/DASHBOARD/i);
  expect(linkElement).toBeInTheDocument();
});
