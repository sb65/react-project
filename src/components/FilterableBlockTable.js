import React from 'react';
import { Tab } from 'semantic-ui-react'
import BlockTable from './BlockTable';

function FilterableBlockTable(props) {
    const nameFilter = props.nameFilter,
        typeFilter = props.typeFilter,
        color = 'tab-bg',
        panes = [
        {
            menuItem: 'In Production',
            render: () => 
            <Tab.Pane className="tab-bg" attached={false}>
                <BlockTable
                    blocksData = {props.blocksData}
                    status = 'Production'
                    nameFilter = {nameFilter}
                    typeFilter = {typeFilter}
                />
            </Tab.Pane>
        },
        {
            menuItem: 'Ready For Production',
            render: () => 
            <Tab.Pane className="tab-bg" attached={false}>
                <BlockTable
                    blocksData = {props.blocksData}
                    status = 'Ready'
                    nameFilter = {nameFilter}
                    typeFilter = {typeFilter}
                />
            </Tab.Pane>
        },
        {
            menuItem: 'Under Development',
            render: () => 
            <Tab.Pane className="tab-bg" attached={false}>
                <BlockTable
                    blocksData = {props.blocksData}
                    status = 'underDev'
                    nameFilter = {nameFilter}
                    typeFilter = {typeFilter}
                />
            </Tab.Pane>
        }
    ]
    return (
        <Tab menu={{color, secondary: true, pointing: true}} panes={panes}/>
    )
}

export default FilterableBlockTable;