import React, { Fragment } from 'react';
import {Table, Icon, Grid} from 'semantic-ui-react';

function VersionDetails(props) {
    const {version, size, coverage, harnessUrl, lastModifiedDate, subscribers, repoURL, blockName, blockOwnerCTO, blockOwnerEmail} = props.versionDetails,
        versionNumber = props.versionNumber,
        creationDate = props.creationDate,
        releaseDate = props.releaseDate,
        versionsCount = props.versionsCount.count,
        blocksCount = props.blocksCount,
        status = props.status,
        blockOwner = blockOwnerEmail[blockOwnerEmail.length - 1],
        blockOwnerHref = "mailto:" + blockOwner;
    
    let subscribersList = '',
        subscribersJSX;
    subscribers && subscribers.map((subscriber, index) => {
        subscribersList = subscribersList.concat(subscriber.name, ', ');
    });
    subscribersList = subscribersList.replace(/,\s*$/, "");
    subscribersJSX = status === 'Production' ? <Table.Cell>{subscribersList}</Table.Cell> : <></>;
    return (
        <Table.Row className="alternate-color">
            {versionNumber === 0 &&
                <React.Fragment>
                    <Table.Cell className="block-number" rowSpan={versionsCount}>{blocksCount}</Table.Cell>
                    <Table.Cell className="block-details" rowSpan={versionsCount}>
                        <Grid>
                            <Grid.Row className="block-name">
                                {blockName}
                            </Grid.Row>
                            <Grid.Row columns={2}>
                                <div className="block-owner">
                                    Block Owner: &nbsp;<a href={blockOwnerHref}>{blockOwner}</a>
                                </div>
                                <div>
                                    Block Owner's CTO: &nbsp;{blockOwnerCTO}
                                </div>
                            </Grid.Row>
                        </Grid>
                        
                    </Table.Cell>   
                </React.Fragment>
            }
            {status === 'underDev' ? (
                <React.Fragment>
                    <Table.Cell>{creationDate}</Table.Cell>
                    <Table.Cell>{releaseDate}</Table.Cell> 
                </React.Fragment>   
            ) : (
                <React.Fragment>
                    <Table.Cell>{version}</Table.Cell>
                    {subscribersJSX}
                    <Table.Cell>{lastModifiedDate}</Table.Cell>
                    <Table.Cell>{size}</Table.Cell>
                    <Table.Cell>{coverage}</Table.Cell>
                    <Table.Cell><a href={repoURL + "/package.json?at=" + version} target="_blank" rel="noopener noreferrer"><Icon name='list ul' /></a></Table.Cell>
                    <Table.Cell><a href={harnessUrl} target="_blank" rel="noopener noreferrer"><Icon name='image outline' /></a></Table.Cell>
                </React.Fragment>
            )}
            
        </Table.Row>
    )
}

export default VersionDetails;