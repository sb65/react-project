import React from 'react';
import {Table, Icon} from 'semantic-ui-react';

function BlockTableHeader(props) {
    const status = props.status,
        consumerJSX = (status === 'Production') ? <Table.HeaderCell rowSpan='2'>Consumers</Table.HeaderCell> : <></>;

    return (
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell rowSpan='2'></Table.HeaderCell>
                <Table.HeaderCell rowSpan='2'>Name <Icon onClick={() => {props.onSort('name')}} name="sort"/></Table.HeaderCell>
                {status === 'underDev' && 
                    <Table.HeaderCell rowSpan='2'>Date Created <Icon onClick={() => {props.onSort('creationDate')}} name="sort"/></Table.HeaderCell>
                }
                {status === 'underDev' ? ( 
                    <Table.HeaderCell rowSpan='2'>Intended Release Date</Table.HeaderCell>
                ) : (
                    <React.Fragment>
                        <Table.HeaderCell rowSpan='2'>Version</Table.HeaderCell>
                        {consumerJSX}
                        <Table.HeaderCell rowSpan='2'>Tag Date <Icon onClick={() => {props.onSort('date')}} name="sort"/></Table.HeaderCell>
                        <Table.HeaderCell rowSpan='2'>Size (Kb) <Icon onClick={() => {props.onSort('size')}} name="sort"/></Table.HeaderCell>
                        <Table.HeaderCell rowSpan='2'>Coverage <Icon onClick={() => {props.onSort('coverage')}} name="sort"/></Table.HeaderCell>
                        <Table.HeaderCell rowSpan='2'>Dependencies</Table.HeaderCell>
                        <Table.HeaderCell rowSpan='2'>Demo Page</Table.HeaderCell>
                    </React.Fragment>
                )}
            </Table.Row>
        </Table.Header>
    )
}

export default BlockTableHeader;