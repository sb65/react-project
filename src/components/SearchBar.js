import _ from 'lodash'
import React from 'react'
import blocksData from '../blocksData.json'

const initialState = { isLoading: '', results: '', value: '' }

export default class SearchBar extends React.Component {

    state = initialState

    filterBlocks = ((blockName) => {
        const matchedBlocks = blocksData.filter(block => {
            return block.blockName.toLowerCase().indexOf(blockName.toLowerCase()) > -1;
        });
        return matchedBlocks.map(block => {
            return block.blockName;
        });
    })

    // handleResultSelect = (e, { result }) => this.setState({ value: result.blockName })

    handleSearchChange = (e) => {
        const value = e.target.value;
        this.setState({ isLoading: 'loading', value })

            setTimeout(() => {
                // if (this.state.value.length < 1) return this.setState(initialState)
                this.setState({
                    isLoading: '',
                    results: value ? this.filterBlocks(this.state.value): ''
                })
                this.props.onFilter(this.state.results)
                console.log(this.state.results);
            }, 300)
    }

    render() {
        const { isLoading, value, results } = this.state

        return (
            <div className={'search-bar ui search ' + this.state.isLoading}>
                <div className="ui icon input">
                    <input
                        className="prompt"
                        type="text"
                        placeholder="Search..."
                        onChange={_.debounce(this.handleSearchChange, 1000, {
                            leading: true,
                        })}
                    />
                    <i className="search icon"></i>
                </div>
                <div className="results"></div>
            </div>
        )
    }
}