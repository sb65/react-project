import React from 'react'
import { Dropdown } from 'semantic-ui-react';
function TypeDropDown(props) {

    const blockTypes = ['Area', 'Controller', 'Component', 'Group'],
        stateOptions = blockTypes.map((type, index) => ({
            key: index,
            text: type,
            value: type.toLowerCase(),
        }));

    return (
        <Dropdown
            onChange={(e, selections) => {props.onTypeChange(selections.value)}}
            // value = {value}
            className=""
            placeholder='Block Type'
            multiple
            clearable
            selection
            options={stateOptions}
        />
    )
}

export default TypeDropDown;