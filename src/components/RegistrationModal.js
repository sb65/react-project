import _ from 'lodash'
import React from 'react'
import { Button, Form, Icon, Input, Modal, TextArea, Select } from 'semantic-ui-react'

class RegistrationModal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            formValid: false,
            formFields: {
                blockName: null,
                blockType: null,
                ownerEmail: null,
                ctoEmail: null,
                requesterSid: null,
                requesterEmail: null,
                teamName: null,
                releaseDate: null,
                description: null,
                extraSids: null
            },
            errors: {
                blockNameErr: true,
                blockTypeErr: true,
                ownerEmailErr: true,
                ctoEmailErr: true,
                requesterSidErr: true,
                requesterEmailErr: true,
                teamNameErr: false,
                releaseDateErr: false,
                descriptionErr: true,
                extraSidsErr: false 
            }
        }
        this.initialState = this.state
    }

    clearForm = () => {
        this.setState(this.initialState);
    }

    validateForm = () => {
        const errors = this.state.errors;
        let fieldError = false;
        Object.keys(errors).forEach(key => {
            fieldError = errors[key] ? true: fieldError
        });
        this.setState({
            formValid: !fieldError
        });
    }

    validateField = (e, selection) => {
        e.preventDefault();
        const value = e.target.value || e.target.innerText,
            {name, required, type} = selection ? selection: e.target;
            
        let errors = Object.assign({}, this.state.errors),
            formFields = Object.assign({}, this.state.formFields);

        formFields[name] = value.trim();
        errors[name + 'Err'] = !value ? required: false
        if (type === 'email') {
            const re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
            errors[name + 'Err'] = !re.test(value);
        }

        this.setState({
            errors: errors,
            formFields: formFields
        }, this.validateForm)
    }
    
    render() {
        const blockTypes = [
            { key: 'area', text: 'Area', value: 'area' },
            { key: 'cntlr', text: 'Controller', value: 'controller' },
            { key: 'cmpt', text: 'Component', value: 'component' },
          ],
          formValid = this.state.formValid,
          {blockNameErr, blockTypeErr, ownerEmailErr, ctoEmailErr, requesterSidErr, requesterEmailErr, teamNameErr, releaseDateErr, descriptionErr, extraSidsErr} = this.state.errors;

        return (
            <Modal closeOnDimmerClick={false} closeIcon onClose={this.clearForm} trigger={<Button primary>Create New Block</Button>}>
                <Modal.Header>Create New Block</Modal.Header>
                <Modal.Content scrolling>
                    <Modal.Description>
                        <Form>
                            <Form.Group widths='equal'>
                                <Form.Field
                                    required
                                    control={Input}
                                    label='Block Name'
                                    placeholder='Block name w/o block type suffix, i.e., no "-area", "-cntlr", or "-cmpt"'
                                    name='blockName'
                                    error={blockNameErr}
                                    onBlur = {(e) => {this.validateField(e)}}
                                />
                                <Form.Field
                                    required
                                    control={Select}
                                    options = {blockTypes}
                                    label='Block Type'
                                    placeholder='Block Type'
                                    name='blockType'
                                    error={blockTypeErr}
                                    onBlur = {(e, selection) => {this.validateField(e, selection)}}
                                />
                            </Form.Group>
                            <Form.Group widths='equal'>
                                <Form.Field
                                    required
                                    control={Input}
                                    label='Block Owner Email'
                                    placeholder='Block Owner Email'
                                    name='ownerEmail'
                                    type='email'
                                    error={ownerEmailErr}
                                    onBlur = {(e) => {this.validateField(e)}}
                                />
                                <Form.Field
                                    required
                                    control={Input}
                                    label="Block Owner's CTO Email"
                                    placeholder="Block Owner's CTO Email"
                                    name='ctoEmail'
                                    type='email'
                                    error={ctoEmailErr}
                                    onBlur = {(e) => {this.validateField(e)}}
                                />
                            </Form.Group>
                            <Form.Group widths='equal'>
                                <Form.Field
                                    required
                                    control={Input}
                                    label='Your SID'
                                    placeholder='Your SID'
                                    name='requesterSid'
                                    error={requesterSidErr}
                                    onBlur = {(e) => {this.validateField(e)}}
                                />
                                <Form.Field
                                    required
                                    control={Input}
                                    label='Your Email'
                                    placeholder='Your Email'
                                    name='requesterEmail'
                                    type='email'
                                    error={requesterEmailErr}
                                    onBlur = {(e) => {this.validateField(e)}}
                                />
                            </Form.Group>
                            <Form.Group widths='equal'>
                                <Form.Field
                                    control={Input}
                                    label='Block Owner Team Name'
                                    placeholder='Block Owner Team Name'
                                    name='teamName'
                                    error={teamNameErr}
                                    onBlur = {(e) => {this.validateField(e)}}
                                />    
                                <Form.Field
                                    control={Input}
                                    label='Intended Release Date'
                                    placeholder='When will you release the first version? e.g. CR420'
                                    name='releaseDate'
                                    error={releaseDateErr}
                                    onBlur = {(e) => {this.validateField(e)}}
                                />
                            </Form.Group>
                            <Form.Field
                                required
                                control={TextArea}
                                label='Block Description'
                                placeholder='What will your block do?'
                                name='description'
                                error={descriptionErr}
                                onBlur = {(e) => {this.validateField(e)}}
                            />
                            <Form.Field
                                control={TextArea}
                                label='Extra SIDs For Write Permission (Comma Separated)'
                                placeholder='Who else is going to work on this block? (SIDs only, separated by comma)'
                                name='extraSids'
                                error={extraSidsErr}
                                onBlur = {(e) => {this.validateField(e)}}
                            />
                        </Form>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <Button disabled={!formValid} primary>
                        Submit <Icon name='chevron right' />
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }
    
}

export default RegistrationModal;