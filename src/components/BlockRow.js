import React from 'react';
import VersionDetails from './VersionDetails';

function BlockRow(props)  {
    const {blockName, blockType, blockOwnerEmail, blockOwnerCTO, repoURL} = props.blockData,
        versionDetailsList = props.blockData.blockVersion,
        releaseDate = props.blockData.intendedReleaseDate,
        creationDate = props.blockData.creationDate,
        versionsCount = versionDetailsList.length,
        blocksCount = props.blocksCount,
        status = props.status,
        versionDetailsRows = [],
        nameFilter = props.nameFilter,
        typeFilter = props.typeFilter,
        filteredByName = nameFilter ? nameFilter.indexOf(blockName) > -1: true,
        filteredByType = Array.isArray(typeFilter) && typeFilter.length > 0 ? typeFilter.indexOf(blockType.toLowerCase()) > -1: true;
    
    let keyNumber = 0,
        versionsCountObj = {
            count:0
        };
    if (versionsCount === 0 && status.toLowerCase() === 'underdev' && filteredByName && filteredByType) {
        versionsCountObj.count++;
        props.incrementBlocksNumber();
        
        return <VersionDetails
                    versionDetails = {{blockName: blockName, blockOwnerEmail:blockOwnerEmail, blockOwnerCTO:blockOwnerCTO}}
                    blocksCount = {blocksCount.count}
                    releaseDate = {releaseDate}
                    creationDate = {creationDate}
                    versionsCount = {versionsCountObj}
                    versionNumber = {0}
                    status = {status}
                />
    } else {
        
        versionDetailsList.map((versionDetails) => {
            const deployment = versionDetails.deployment,
                showBlocks = versionsCount > 0 ? status.toLowerCase() === deployment.toLowerCase(): status.toLowerCase() === 'underdev';
                
            versionDetails.repoURL = repoURL;
            versionDetails.blockName = blockName;
            versionDetails.blockOwnerCTO = blockOwnerCTO;
            versionDetails.blockOwnerEmail = blockOwnerEmail;
            if (showBlocks && filteredByName && filteredByType) {
                versionsCountObj.count === 0 && props.incrementBlocksNumber();
                versionsCountObj.count++;
                versionDetailsRows.push(
                    <VersionDetails
                        versionDetails = {versionDetails}
                        blocksCount = {blocksCount.count}
                        versionNumber = {keyNumber}
                        versionsCount = {versionsCountObj}
                        status = {status}
                        key = {keyNumber}
                    />
                )
                keyNumber++;
            }
        });
    
        return versionDetailsRows
    }
}

export default BlockRow;