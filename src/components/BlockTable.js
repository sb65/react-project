import React from 'react';
import {Table} from 'semantic-ui-react';
import BlockTableHeader from './BlockTableHeader';
import BlockRow from './BlockRow';

class BlockTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            blocksData: this.props.blocksData,
            sortBy: '',
            sortDirection: 1
        }
        this.blocksCountObj = {
            count: 0
        };
    }

    resetBlocksNumber() {
        this.blocksCountObj.count = 0;
    }

    incrementBlocksNumber() {
        this.blocksCountObj.count++;
    }

    handleSort(sortBy) {
        let blocksData = this.state.blocksData.slice();
        const sortDirection = this.state.sortDirection;
        blocksData.sort(this.compareSort(sortBy));
        this.setState({
            blocksData: blocksData,
            sortDirection: -1 * sortDirection
        });
    }
    
    compareSort(sortBy) {
        const sortDirection = this.state.sortDirection;
        return function(a, b) {
            if (sortBy === 'name') {
                return sortDirection * a.blockName.localeCompare(b.blockName);
            } else if (sortBy === 'date') {
                var timeA = Array.isArray(a.blockVersion) && a.blockVersion.length > 0 ? new Date(a.blockVersion[0].lastModifiedDate): new Date(0),
                    timeB = Array.isArray(b.blockVersion) && b.blockVersion.length > 0 ? new Date(b.blockVersion[0].lastModifiedDate): new Date(0);
                return sortDirection * (timeA.getTime() - timeB.getTime())
            } else if (sortBy === 'size') {
                const aSize = Array.isArray(a.blockVersion) && a.blockVersion.length > 0 ? a.blockVersion[0].size: '0',
                    bSize = Array.isArray(b.blockVersion) && b.blockVersion.length > 0 ? b.blockVersion[0].size: '0';
                return sortDirection * aSize.localeCompare(bSize, undefined, { numeric: true });
            } else if (sortBy === 'coverage') {
                const aCoverage = Array.isArray(a.blockVersion) && a.blockVersion.length > 0 ? a.blockVersion[0].coverage: '0',
                    bCoverage = Array.isArray(b.blockVersion) && b.blockVersion.length > 0 ? b.blockVersion[0].coverage: '0';
                return sortDirection * aCoverage.localeCompare(bCoverage, undefined, { numeric: true });
            } else if (sortBy === 'creationDate') {
                var timeA = a.creationDate ? new Date(a.creationDate): new Date(0),
                    timeB = b.creationDate ? new Date(b.creationDate): new Date(0);
                return sortDirection * (timeA.getTime() - timeB.getTime())
            } else {
                return 1;
            }
        }
    }


    render() {
        const rows = [],
            blocksData = this.state.blocksData,
            status = this.props.status,
            nameFilter = this.props.nameFilter,
            typeFilter = this.props.typeFilter;
        let keyNumber = 0;
        this.resetBlocksNumber();
        blocksData && blocksData.map((block) => {
            rows.push(
                <BlockRow
                    blockData = {block}
                    status = {status}
                    key = {keyNumber}
                    blocksCount = {this.blocksCountObj}
                    incrementBlocksNumber = {() => {this.incrementBlocksNumber()}}
                    nameFilter = {nameFilter}
                    typeFilter = {typeFilter}
                />
            )
            keyNumber++;
        })
        return (
            <Table celled structured striped>
                <BlockTableHeader
                    status = {status}
                    onSort = {(sortBy) => {this.handleSort(sortBy)}}
                />
                <Table.Body>
                    {rows}
                </Table.Body>
            </Table>
        )
    }
}

export default BlockTable;