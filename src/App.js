import React from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css'
import FilterableBlockTable from './components/FilterableBlockTable';
import blocksData from './blocksData.json'
import { Grid, Button } from 'semantic-ui-react';
import SearchBar from './components/SearchBar';
import TypeDropDown from './components/TypeDropDown';
import RegistrationModal from './components/RegistrationModal';

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      nameFilter: undefined,
      typeFilter: undefined
    }
  };

  handleNameFilter = (nameFilter => {
    this.setState({
      nameFilter: nameFilter
    })
  });

  handleTypeFilter = (typeFilter => {
    this.setState({
      typeFilter: typeFilter
    })
  });

  render() {
    return (
      <div className="App">
        <Grid stackable>
          <Grid.Row className="top-bar">
            <div className="logo-container">
              <span className="logo"></span>
              <h2 className="page-title">BLOCKS <span>DASHBOARD</span></h2>
            </div>
          </Grid.Row>
          <Grid.Row columns={4} className='filter-bar'>
            <div className="left floated column left-group-actions">
              <div className="search-bar">
                <SearchBar
                  onFilter = {(nameFilter) => {this.handleNameFilter(nameFilter)}}
                />
              </div>
              <div>
                <TypeDropDown
                  onTypeChange = {(typeFilter) => {this.handleTypeFilter(typeFilter)}}
                />
              </div>
            </div>
            <div className="right-group-actions">
              <div>
                <RegistrationModal/>
              </div>
              <div>
                <a target="_blank" rel="noopener noreferrer" href="https://confluence.jpmchase.net/confluence2/display/BLUES/BlueBlock+Governance"><Button className="teal">Documentation</Button></a>
              </div>
            </div>
          </Grid.Row>
          <Grid.Row className="table">
            <Grid.Column>
              <FilterableBlockTable
                blocksData = {blocksData}
                nameFilter = {this.state.nameFilter}
                typeFilter = {this.state.typeFilter}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row columns={2} className="footer">
              <div className="left floated column left-group-footer">
                  Please contact &nbsp;<a href="mailto:blue_blocks_governance@restricted.chase.com">blue_blocks_governance@restricted.chase.com</a>&nbsp; with your feedback.
              </div>
              <div className="right-group-footer">
                  2017 | JPMorgan Chase Internal Use Only. All Rights Reserved
              </div>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default App;
